import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/ordens/novas',
    name: 'order_new',
    component: () => import('../views/order/OrderNew.vue')
  },
  {
    path: '/ordens/processadas',
    name: 'order_processed',
    component: () => import('../views/order/OrderProcessed.vue')
  },
  {
    path: '/ordens/retornadas',
    name: 'order_returned',
    component: () => import('../views/order/OrderReturned.vue')
  },
  {
    path: '/cadastros/contabilista',
    name: 'accountant',
    component: () => import('../views/registration/Accountant.vue')
  },
  {
    path: '/cadastros/contabilista/novo',
    name: 'accountantNew',
    component: () => import('../views/registration/AccountantNew.vue')
  },
  {
    path: '/foundation/empresa',
    name: 'company',
    component: () => import('../views/foundation/Company.vue')
  },
  {
    path: '/foundation/monitor_processamento',
    name: 'processing_monitor',
    component: () => import('../views/foundation/ProcessingMonitor.vue')
  },
  {
    path: '/exemplo',
    name: 'sample',
    component: () => import('../views/Sample.vue')
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
